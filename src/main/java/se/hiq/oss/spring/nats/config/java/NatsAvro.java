package se.hiq.oss.spring.nats.config.java;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({NatsAvroConfiguration.class, NatsConfiguration.class})
public class NatsAvro {
}
