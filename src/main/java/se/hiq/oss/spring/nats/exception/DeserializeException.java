package se.hiq.oss.spring.nats.exception;

public class DeserializeException extends RuntimeException {
    public DeserializeException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
