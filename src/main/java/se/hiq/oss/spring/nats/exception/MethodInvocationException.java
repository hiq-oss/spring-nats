package se.hiq.oss.spring.nats.exception;

public class MethodInvocationException extends RuntimeException {
    public MethodInvocationException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
