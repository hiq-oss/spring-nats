package se.hiq.oss.spring.nats.exception;

public class RequestFailedException extends RuntimeException {
    public RequestFailedException(final String message) {
        super(message);
    }

    public RequestFailedException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
