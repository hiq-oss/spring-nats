package se.hiq.oss.spring.nats.message.serde;

import java.util.Optional;

import se.hiq.oss.spring.nats.exception.SerializeException;
import se.hiq.oss.spring.nats.message.validation.MessageObjectValidator;

public interface NatsMessageSerializer {
    void setValidator(Optional<MessageObjectValidator> validator);

    byte[] toMessageData(Object object) throws SerializeException;
}
