package se.hiq.oss.spring.nats.message.serde;

import se.hiq.oss.spring.nats.message.validation.MessageObjectValidator;

public interface NatsMessageSerDeFactory {
    NatsMessageDeserializer createDeserializer(Class<?> forClass);

    NatsMessageSerializer createSerializer();

    void setValidator(MessageObjectValidator validator);
}
