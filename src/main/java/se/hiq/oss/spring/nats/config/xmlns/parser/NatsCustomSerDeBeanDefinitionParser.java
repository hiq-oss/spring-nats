package se.hiq.oss.spring.nats.config.xmlns.parser;

import java.util.Optional;

import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.xml.ParserContext;

import org.w3c.dom.Element;

import static se.hiq.oss.spring.nats.config.NatsBeans.SER_DE_FACTORY;
import se.hiq.oss.spring.nats.config.xmlns.NatsCustomSerDe;

public class NatsCustomSerDeBeanDefinitionParser extends AbstractNatsBeanDefinitionParser {
    @Override
    protected Class<?> getBeanClass(Element element) {
        return NatsCustomSerDe.class;
    }

    @Override
    protected void doParse(Element element, ParserContext pc,
                           BeanDefinitionBuilder bean) {
        element.setAttribute(ID_ATTRIBUTE, SER_DE_FACTORY);
        element.setAttribute(NAME_ATTRIBUTE, SER_DE_FACTORY);
        Optional<String> serDeFactory = getAttributeValue(element, "message-serde-factory");
        if (!serDeFactory.isPresent()) {
            throw new IllegalStateException("Missing required attribute message-serde-factory");
        } else {
            createBeans(pc, element, serDeFactory.get());
        }
    }

}
