package se.hiq.oss.spring.nats.config.java;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import static se.hiq.oss.spring.nats.config.NatsBeans.SER_DE_FACTORY;
import se.hiq.oss.spring.nats.message.serde.json.NatsJacksonMessageSerDeFactory;
import se.hiq.oss.spring.nats.message.validation.JsonSchemaValidator;
import se.hiq.oss.spring.nats.message.validation.MessageObjectValidator;

@Configuration
public class NatsJacksonConfiguration {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private Optional<MessageObjectValidator> validator;

    @Autowired
    private Optional<JsonSchemaValidator> jsonSchemaValidator;


    @Bean(name = SER_DE_FACTORY)
    public NatsJacksonMessageSerDeFactory natsJacksonMessageSerDeFactory() {
        NatsJacksonMessageSerDeFactory natsJacksonMessageSerDeFactory = new NatsJacksonMessageSerDeFactory();

        natsJacksonMessageSerDeFactory.setObjectMapper(objectMapper);
        jsonSchemaValidator.ifPresent(v -> natsJacksonMessageSerDeFactory.setSchemaValidator(v));
        validator.ifPresent(v -> natsJacksonMessageSerDeFactory.setValidator(v));

        return natsJacksonMessageSerDeFactory;
    }




}
