package se.hiq.oss.spring.nats.exception;

public class RequestTimeoutException extends RuntimeException {
    public RequestTimeoutException(final String message) {
        super(message);
    }

    public RequestTimeoutException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
