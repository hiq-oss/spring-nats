package se.hiq.oss.spring.nats.config.xmlns;

import org.springframework.beans.factory.xml.NamespaceHandlerSupport;

import se.hiq.oss.spring.nats.config.xmlns.parser.ConnectionBeanDefinitionParser;
import se.hiq.oss.spring.nats.config.xmlns.parser.NatsAvroBeanDefinitionParser;
import se.hiq.oss.spring.nats.config.xmlns.parser.NatsCustomSerDeBeanDefinitionParser;
import se.hiq.oss.spring.nats.config.xmlns.parser.NatsGsonBeanDefinitionParser;
import se.hiq.oss.spring.nats.config.xmlns.parser.NatsJacksonBeanDefinitionParser;
import se.hiq.oss.spring.nats.config.xmlns.parser.NatsJavaBeanDefinitionParser;
import se.hiq.oss.spring.nats.config.xmlns.parser.NatsJaxbBeanDefinitionParser;
import se.hiq.oss.spring.nats.config.xmlns.parser.NatsKryoBeanDefinitionParser;
import se.hiq.oss.spring.nats.config.xmlns.parser.NatsProtobufBeanDefinitionParser;
import se.hiq.oss.spring.nats.config.xmlns.parser.NatsThriftBeanDefinitionParser;

@SuppressWarnings("checkstyle:classdataabstractioncoupling")
public class NatsXmlNamespaceHandler extends NamespaceHandlerSupport {
    @Override
    public void init() {
        registerBeanDefinitionParser("connection", new ConnectionBeanDefinitionParser());
        registerBeanDefinitionParser("jackson", new NatsJacksonBeanDefinitionParser());
        registerBeanDefinitionParser("gson", new NatsGsonBeanDefinitionParser());
        registerBeanDefinitionParser("java", new NatsJavaBeanDefinitionParser());
        registerBeanDefinitionParser("protobuf", new NatsProtobufBeanDefinitionParser());
        registerBeanDefinitionParser("thrift", new NatsThriftBeanDefinitionParser());
        registerBeanDefinitionParser("avro", new NatsAvroBeanDefinitionParser());
        registerBeanDefinitionParser("jaxb", new NatsJaxbBeanDefinitionParser());
        registerBeanDefinitionParser("kryo", new NatsKryoBeanDefinitionParser());
        registerBeanDefinitionParser("custom", new NatsCustomSerDeBeanDefinitionParser());
    }
}
