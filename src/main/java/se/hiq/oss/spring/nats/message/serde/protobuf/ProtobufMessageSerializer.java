package se.hiq.oss.spring.nats.message.serde.protobuf;

import java.util.Optional;

import com.google.protobuf.Message;

import se.hiq.oss.spring.nats.exception.SerializeException;
import se.hiq.oss.spring.nats.message.serde.NatsMessageSerializer;
import se.hiq.oss.spring.nats.message.validation.MessageObjectValidator;

public class ProtobufMessageSerializer implements NatsMessageSerializer {
    private Optional<MessageObjectValidator> validator = Optional.empty();

    @Override
    public void setValidator(Optional<MessageObjectValidator> validator) {
        this.validator = validator;
    }

    @Override
    public byte[] toMessageData(Object object) throws SerializeException {
        validator.filter(v -> v.shouldValidate(object.getClass())).ifPresent(v -> v.validate(object));
        return ((Message) object).toByteArray();
    }
}
