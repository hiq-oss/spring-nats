package se.hiq.oss.spring.nats.event;

import org.springframework.context.ApplicationEvent;

import io.nats.client.Connection;
import io.nats.client.Consumer;


public class NatsSlowConsumerEvent extends ApplicationEvent {

    private Consumer consumer;
    /**
     * Create a new NatsSlowConsumerEvent.
     *
     * @param connection the object on which the event initially occurred (never {@code null})
     */
    public NatsSlowConsumerEvent(final Connection connection,
                                 final Consumer consumer) {
        super(connection);
        this.consumer = consumer;
    }

    @Override
    public Connection getSource() {
        return (Connection) super.getSource();
    }

    public Consumer getConsumer() {
        return consumer;
    }
}
