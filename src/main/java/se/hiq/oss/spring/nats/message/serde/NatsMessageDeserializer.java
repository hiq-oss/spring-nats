package se.hiq.oss.spring.nats.message.serde;

import java.util.Optional;

import io.nats.client.Message;

import se.hiq.oss.spring.nats.exception.DeserializeException;
import se.hiq.oss.spring.nats.message.validation.MessageObjectValidator;

public interface NatsMessageDeserializer {

    void setValidator(Optional<MessageObjectValidator> validator);

    Object fromMessage(Message message) throws DeserializeException;
}
