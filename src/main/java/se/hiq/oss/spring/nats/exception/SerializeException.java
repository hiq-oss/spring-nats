package se.hiq.oss.spring.nats.exception;

public class SerializeException extends RuntimeException {
    public SerializeException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
