package se.hiq.oss.spring.nats.config.java;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import static se.hiq.oss.spring.nats.config.NatsBeans.SER_DE_FACTORY;
import se.hiq.oss.spring.nats.message.serde.avro.AvroMessageSerDeFactory;
import se.hiq.oss.spring.nats.message.validation.MessageObjectValidator;

@Configuration
public class NatsAvroConfiguration {

    @Autowired
    private Optional<MessageObjectValidator> validator;

    @Bean(name = SER_DE_FACTORY)
    public AvroMessageSerDeFactory avroMessageSerDeFactory() {
        AvroMessageSerDeFactory avroMessageSerDeFactory = new AvroMessageSerDeFactory();
        validator.ifPresent(v -> avroMessageSerDeFactory.setValidator(v));
        return avroMessageSerDeFactory;
    }
}
