package se.hiq.oss.spring.nats.message.validation;

import java.nio.charset.StandardCharsets;
import java.util.Optional;

import se.hiq.oss.json.schema.repo.JsonSchemaRegistration;
import se.hiq.oss.json.schema.repo.JsonSchemaRepository;

public class JsonSchemaValidator implements MessageDataValidator {

    private JsonSchemaRepository jsonSchemaRepository;

    public JsonSchemaValidator(final JsonSchemaRepository jsonSchemaRepository) {
        this.jsonSchemaRepository = jsonSchemaRepository;
    }

    @Override
    public void validate(byte[] data, Class<?> ofType) {
        Optional<JsonSchemaRegistration> schemaRegistration = jsonSchemaRepository.getSchemaRegistration(ofType);
        schemaRegistration.ifPresent((s) -> s.getStringValidator().validate(new String(data, StandardCharsets.UTF_8)));
    }
}
