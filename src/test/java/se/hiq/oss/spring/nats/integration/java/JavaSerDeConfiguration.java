package se.hiq.oss.spring.nats.integration.java;

import javax.validation.Validation;
import javax.validation.Validator;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import org.hibernate.validator.messageinterpolation.ParameterMessageInterpolator;
import se.hiq.oss.spring.nats.config.java.NatsJava;
import se.hiq.oss.spring.nats.message.validation.BeanValidationValidator;
import se.hiq.oss.spring.nats.message.validation.MessageObjectValidator;

@Import(NatsJava.class)
@Configuration
public class JavaSerDeConfiguration {

    @Bean
    public ProjectListener projectListener() {
        return new ProjectListener();
    }

    @Bean
    public MessageObjectValidator messageObjectValidator() {
        Validator validator = Validation.byDefaultProvider()
                .configure()
                .messageInterpolator(new ParameterMessageInterpolator())
                .buildValidatorFactory()
                .getValidator();
        return new BeanValidationValidator(validator);
    }
}
