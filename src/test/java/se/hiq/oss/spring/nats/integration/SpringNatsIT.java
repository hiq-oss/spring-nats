package se.hiq.oss.spring.nats.integration;

import java.io.File;

import org.springframework.util.SocketUtils;

import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.testcontainers.containers.DockerComposeContainer;
import org.testcontainers.containers.wait.strategy.Wait;
import se.hiq.oss.spring.nats.integration.avro.NatsAvroJavaIntegration;
import se.hiq.oss.spring.nats.integration.avro.NatsAvroXmlIntegration;
import se.hiq.oss.spring.nats.integration.custom.NatsCustomJavaIntegration;
import se.hiq.oss.spring.nats.integration.custom.NatsCustomXmlIntegration;
import se.hiq.oss.spring.nats.integration.java.NatsJavaSerDeJavaIntegration;
import se.hiq.oss.spring.nats.integration.java.NatsJavaSerDeXmlIntegration;
import se.hiq.oss.spring.nats.integration.json.NatsGsonJavaIntegration;
import se.hiq.oss.spring.nats.integration.json.NatsGsonXmlIntegration;
import se.hiq.oss.spring.nats.integration.json.NatsJacksonJavaIntegration;
import se.hiq.oss.spring.nats.integration.json.NatsJacksonXmlIntegration;
import se.hiq.oss.spring.nats.integration.kryo.NatsKryoJavaIntegration;
import se.hiq.oss.spring.nats.integration.kryo.NatsKryoXmlIntegration;
import se.hiq.oss.spring.nats.integration.proto.NatsProtoJavaIntegration;
import se.hiq.oss.spring.nats.integration.proto.NatsProtoXmlIntegration;
import se.hiq.oss.spring.nats.integration.thrift.NatsThriftJavaIntegration;
import se.hiq.oss.spring.nats.integration.thrift.NatsThriftXmlIntegration;
import se.hiq.oss.spring.nats.integration.xml.NatsJaxbJavaIntegration;
import se.hiq.oss.spring.nats.integration.xml.NatsJaxbXmlIntegration;


@RunWith(Suite.class)
@Suite.SuiteClasses({
                            NatsJacksonJavaIntegration.class,
                            NatsJacksonXmlIntegration.class,
                            NatsGsonXmlIntegration.class,
                            NatsGsonJavaIntegration.class,
                            NatsKryoJavaIntegration.class,
                            NatsKryoXmlIntegration.class,
                            NatsJavaSerDeJavaIntegration.class,
                            NatsJavaSerDeXmlIntegration.class,
                            NatsAvroXmlIntegration.class,
                            NatsAvroJavaIntegration.class,
                            NatsProtoXmlIntegration.class,
                            NatsProtoJavaIntegration.class,
                            NatsThriftJavaIntegration.class,
                            NatsThriftXmlIntegration.class,
                            NatsJaxbXmlIntegration.class,
                            NatsJaxbJavaIntegration.class,
                            NatsCustomXmlIntegration.class,
                            NatsCustomJavaIntegration.class
                    })
public class SpringNatsIT {
    public static final Integer NATS_PORT = SocketUtils.findAvailableTcpPort(10000);
    public static final Integer NATS_MONITOR_PORT = SocketUtils.findAvailableTcpPort(10000);


    @ClassRule
    public static DockerComposeContainer dockerComposeContainer =
            new DockerComposeContainer(new File("src/test/resources/docker/docker-compose.yml"))
                    .withEnv("NATS_PORT", NATS_PORT.toString())
                    .withEnv("NATS_MONITOR_PORT", NATS_MONITOR_PORT.toString())
                    .waitingFor("nats", Wait.forListeningPort())
                    .waitingFor("nats", Wait.forLogMessage(".*Server is ready.*\\n", 1));

    @BeforeClass
    public static void setupPorts() {
        System.setProperty("NATS_PORT", NATS_PORT.toString());
        System.setProperty("NATS_MONITOR_PORT", NATS_MONITOR_PORT.toString());
        System.out.println("\n\n##################################");
        System.out.println("Starting NATS with port " + NATS_PORT + " and monitoring port " + NATS_MONITOR_PORT);
        System.out.println("##################################\n\n");
    }

}
