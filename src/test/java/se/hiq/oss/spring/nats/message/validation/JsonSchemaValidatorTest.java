package se.hiq.oss.spring.nats.message.validation;

import java.util.Optional;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import se.hiq.oss.json.schema.repo.JsonSchemaRegistration;
import se.hiq.oss.json.schema.repo.JsonSchemaRepository;


@RunWith(MockitoJUnitRunner.class)
public class JsonSchemaValidatorTest {

    @Mock
    private JsonSchemaRepository repository;

    @Mock
    private JsonSchemaRegistration schemaRegistration;

    @Mock
    private se.hiq.oss.json.schema.validation.impl.JsonSchemaValidator<String> stringJsonSchemaValidator;


    private JsonSchemaValidator validator;


    @Before
    public void setup() {
        validator = new JsonSchemaValidator(repository);
    }

    @Test
    public void nothingInRepository() {
        byte[] data = {};

        when(repository.getSchemaRegistration(Object.class)).thenReturn(Optional.empty());

        validator.validate(data, Object.class);
    }

    @Test
    public void validate() {
        byte[] data = {};

        when(repository.getSchemaRegistration(Object.class)).thenReturn(Optional.of(schemaRegistration));
        when(schemaRegistration.getStringValidator()).thenReturn(stringJsonSchemaValidator);

        validator.validate(data, Object.class);

        verify(stringJsonSchemaValidator).validate("");
    }



}
