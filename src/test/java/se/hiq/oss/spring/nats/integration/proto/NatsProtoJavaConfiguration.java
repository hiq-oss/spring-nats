package se.hiq.oss.spring.nats.integration.proto;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import se.hiq.oss.spring.nats.config.java.NatsProtobuf;

@Import(NatsProtobuf.class)
@Configuration
public class NatsProtoJavaConfiguration {

    @Bean
    public SimpleMeterRegistry simpleMeterRegistry() {
        return new SimpleMeterRegistry();
    }
}
