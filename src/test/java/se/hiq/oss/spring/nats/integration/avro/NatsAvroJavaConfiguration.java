package se.hiq.oss.spring.nats.integration.avro;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import io.nats.client.Connection;
import se.hiq.oss.spring.nats.config.java.NatsAvro;
import se.hiq.oss.spring.nats.event.NatsErrorListener;
import se.hiq.oss.spring.nats.metrics.NatsMetricsRegistry;

@Import(NatsAvro.class)
@Configuration
public class NatsAvroJavaConfiguration {

    @Bean
    public NatsErrorListener errorListener() {
        return new NatsErrorListener();
    }

    @Bean
    public SimpleMeterRegistry simpleMeterRegistry() {
        return new SimpleMeterRegistry();
    }

    @Bean
    public NatsMetricsRegistry natsMetricsRegistry(Connection connection) {
        return new NatsMetricsRegistry(simpleMeterRegistry(), connection);
    }
}
