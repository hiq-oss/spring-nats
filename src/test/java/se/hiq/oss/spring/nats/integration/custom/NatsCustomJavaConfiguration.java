package se.hiq.oss.spring.nats.integration.custom;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import se.hiq.oss.spring.nats.config.java.NatsConfiguration;

@Import(NatsConfiguration.class)
@Configuration
public class NatsCustomJavaConfiguration {
    @Bean
    public CustomSerDeFactory customSerDeFactory() {
        return new CustomSerDeFactory();
    }
}
