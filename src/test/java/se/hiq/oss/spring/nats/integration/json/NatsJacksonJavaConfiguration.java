package se.hiq.oss.spring.nats.integration.json;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.fasterxml.jackson.databind.ObjectMapper;
import se.hiq.oss.spring.nats.config.java.JsonSchemaConfiguration;
import se.hiq.oss.spring.nats.config.java.NatsJackson;


@Import({NatsJackson.class, JsonSchemaConfiguration.class})
@Configuration
public class NatsJacksonJavaConfiguration {

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }
}
